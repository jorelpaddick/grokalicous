# Predefined Grok Patterns for Linux Log Files

## alternatives log 
```
%{NOTSPACE:command}%{YEAR:year}-%{MONTHNUM:month}-%{MONTHDAY:day} %{HOUR:hour}:%{MINUTE:min}:%{SECOND:sec}: %{GREEDYDATA:message}
```

## syslog (/var/syslog)
```
%{SYSLOGTIMESTAMP:timestamp} %{HOSTNAME:hostname} %{WORD:process}: %{GREEDYDATA:message}
```

## Apache Access Log
```
%{IP:clientIP} - - \[%{MONTHDAY}/%{MONTH}/%{YEAR}:%{TIME} %{ISO8601_TIMEZONE}\] "%{WORD:requestType} %{NOTSPACE:url} %{NOTSPACE:protocol}" %{NUMBER:statusCode} %{NUMBER} "%{NOTSPACE:posturl}" "%{GREEDYDATA:clientinfo} 
```

## Apache Error Log
```
\[%{DAY} %{MONTH} %{MONTHDAY} %{HOUR}:%{MINUTE}:%{SECOND} %{YEAR}\] \[%{WORD:module}:%{WORD:logLevel}\] \[pid %{NUMBER:pid}\] \[client %{IP:client}:%{NUMBER:tid}\] %{GREEDYDATA:message}
```

##  apt history log
```
Start-Date: %{YEAR:startYear}-%{MONTHNUM:startMonth}-%{MONTHDAY:startDay}  %{TIME:startTime}
Commandline: %{GREEDYDATA:command}
Requested-By: %{USER:user} \(%{NUMBER:userid}\)
Install: %{GREEDYDATA:pacakages}
End-Date: %{YEAR:endYear}-%{MONTHNUM:endMonth}-%{MONTHDAY:endDay}  %{TIME:endTime}
```

## apt terminal log
```
%{GREEDYDATA:message}
```

## auth.log
```
%{MONTH:month} %{MONTHDAY:day} %{HOUR:hour}:%{MINUTE:min}:%{SECOND:sec} %{HOSTNAME} %{WORD:process}\[%{NUMBER:pid}\]: %{GREEDYDATA:message}
```

## bootstrap.log
```
%{GREEDYDATA:message}
```

## dpkg.log
```
%{DATE: date} %{TIME} %{WORD:messageType} %{GREEDYDATA:message}
```

## kern.log
```
%{MONTH} %{MONTHDAY} %{TIME} %{HOSTNAME} %{WORD:program}: \[%{SPACE}%{NUMBER}\] %{GREEDYDATA:message}
```

## MySQL Error Log
```
%{TIMESTAMP_ISO8601} %{NUMBER:type} \[%{WORD:level}\] %{GREEDYDATA:message}
```

